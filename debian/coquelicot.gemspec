# -*- encoding: utf-8 -*-
# stub: coquelicot 0.9.6 ruby lib

Gem::Specification.new do |s|
  s.name = "coquelicot".freeze
  s.version = "0.9.6"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["potager.org".freeze, "mh / immerda.ch".freeze]
  s.date = "2016-12-20"
  s.description = "Coquelicot is a \"one-click\" file sharing web application with a\nfocus on protecting users' privacy.\n\nBasic principle: users can upload a file to the server, in return they\nget a unique URL which can be shared with others in order to download\nthe file.\n\nCoquelicot aims to protect, to some extent, users and system\nadministrators from disclosure of the files exchanged from passive and\nnot so active attackers.\n".freeze
  s.email = ["jardiniers@potager.org".freeze]
  s.executables = ["coquelicot".freeze]
  s.files = ["Gemfile".freeze, "Gemfile.lock".freeze, "HACKING".freeze, "INSTALL".freeze, "LICENSE".freeze, "NEWS".freeze, "README".freeze, "Rakefile".freeze, "TODO".freeze, "bin/coquelicot".freeze, "conf/settings-default.yml".freeze, "conf/settings-imap.yml".freeze, "conf/settings-ldap.yml".freeze, "conf/settings-simplepass.yml".freeze, "conf/settings-userpass.yml".freeze, "coquelicot.gemspec".freeze, "features/auth/imap.feature".freeze, "features/auth/ldap.feature".freeze, "features/auth/simplepass.feature".freeze, "features/auth/userpass.feature".freeze, "features/download.feature".freeze, "features/download_passwords.feature".freeze, "features/expiration.feature".freeze, "features/i18n.feature".freeze, "features/links.feature".freeze, "features/one_time_downloads.feature".freeze, "features/step_definitions/auth/imap.rb".freeze, "features/step_definitions/auth/ldap.rb".freeze, "features/step_definitions/auth/simplepass.rb".freeze, "features/step_definitions/auth/userpass.rb".freeze, "features/step_definitions/downloads.rb".freeze, "features/step_definitions/settings.rb".freeze, "features/step_definitions/storage.rb".freeze, "features/step_definitions/uploads.rb".freeze, "features/step_definitions/urls.rb".freeze, "features/step_definitions/web.rb".freeze, "features/storage.feature".freeze, "features/support/env.rb".freeze, "features/support/hooks.rb".freeze, "features/upload_restrictions.feature".freeze, "lib/coquelicot.rb".freeze, "lib/coquelicot/app.rb".freeze, "lib/coquelicot/auth.rb".freeze, "lib/coquelicot/auth/imap.rb".freeze, "lib/coquelicot/auth/ldap.rb".freeze, "lib/coquelicot/auth/simplepass.rb".freeze, "lib/coquelicot/auth/userpass.rb".freeze, "lib/coquelicot/base_app.rb".freeze, "lib/coquelicot/debian.rb".freeze, "lib/coquelicot/depot.rb".freeze, "lib/coquelicot/helpers.rb".freeze, "lib/coquelicot/jyraphe_migrator.rb".freeze, "lib/coquelicot/num.rb".freeze, "lib/coquelicot/rack/multipart_parser.rb".freeze, "lib/coquelicot/rack/upload.rb".freeze, "lib/coquelicot/stored_file.rb".freeze, "lib/coquelicot/version.rb".freeze, "po/coquelicot.pot".freeze, "po/de/coquelicot.po".freeze, "po/el/coquelicot.po".freeze, "po/es/coquelicot.po".freeze, "po/fr/coquelicot.po".freeze, "public/images/ajax-loader.gif".freeze, "public/images/blank.gif".freeze, "public/images/overlay.png".freeze, "public/javascripts/coquelicot.auth.imap.js".freeze, "public/javascripts/coquelicot.auth.ldap.js".freeze, "public/javascripts/coquelicot.auth.simplepass.js".freeze, "public/javascripts/coquelicot.auth.userpass.js".freeze, "public/javascripts/coquelicot.js".freeze, "public/javascripts/jquery.lightBoxFu.js".freeze, "public/javascripts/jquery.min.js".freeze, "public/javascripts/jquery.uploadProgress.js".freeze, "public/stylesheets/lightbox-fu-ie6.css".freeze, "public/stylesheets/lightbox-fu-ie7.css".freeze, "spec/coquelicot/app_spec.rb".freeze, "spec/coquelicot/auth/imap_spec.rb".freeze, "spec/coquelicot/auth/ldap_spec.rb".freeze, "spec/coquelicot/auth/simplepass_spec.rb".freeze, "spec/coquelicot/auth/userpass_spec.rb".freeze, "spec/coquelicot/depot_spec.rb".freeze, "spec/coquelicot/jyraphe_migrator_spec.rb".freeze, "spec/coquelicot/rack/multipart_parser_spec.rb".freeze, "spec/coquelicot/rack/upload_spec.rb".freeze, "spec/coquelicot/stored_file_spec.rb".freeze, "spec/fixtures/LICENSE-secret-1.0/reference".freeze, "spec/fixtures/LICENSE-secret-1.0/stored_file".freeze, "spec/fixtures/LICENSE-secret-2.0/reference".freeze, "spec/fixtures/LICENSE-secret-2.0/stored_file".freeze, "spec/fixtures/LICENSE-secret-2.0/stored_file.content".freeze, "spec/fixtures/small-secret-1.0/reference".freeze, "spec/fixtures/small-secret-1.0/stored_file".freeze, "spec/spec_helper.rb".freeze, "views/about_your_data.haml".freeze, "views/auth/imap.haml".freeze, "views/auth/ldap.haml".freeze, "views/auth/simplepass.haml".freeze, "views/auth/userpass.haml".freeze, "views/download_in_progress.haml".freeze, "views/enter_file_key.haml".freeze, "views/error.haml".freeze, "views/expired.haml".freeze, "views/forbidden.haml".freeze, "views/index.haml".freeze, "views/layout.haml".freeze, "views/not_found.haml".freeze, "views/ready.haml".freeze, "views/style.sass".freeze]
  s.homepage = "https://coquelicot.potager.org/".freeze
  s.licenses = ["AGPL-3.0".freeze]
  s.rubygems_version = "2.5.2".freeze
  s.summary = "\"one-click\" file sharing web application focusing on privacy".freeze

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rake>.freeze, [">= 0"])
      s.add_development_dependency(%q<rspec>.freeze, ["~> 3"])
      s.add_development_dependency(%q<timecop>.freeze, [">= 0"])
      s.add_development_dependency(%q<rack-test>.freeze, [">= 0"])
      s.add_development_dependency(%q<capybara>.freeze, [">= 0"])
      s.add_development_dependency(%q<cucumber>.freeze, [">= 0"])
      s.add_development_dependency(%q<activesupport>.freeze, [">= 0"])
      s.add_development_dependency(%q<tzinfo>.freeze, [">= 0"])
      s.add_development_dependency(%q<net-ldap>.freeze, [">= 0"])
      s.add_development_dependency(%q<gettext>.freeze, ["~> 3"])
      s.add_development_dependency(%q<bcrypt>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<sinatra>.freeze, ["~> 1.4"])
      s.add_runtime_dependency(%q<sinatra-contrib>.freeze, ["~> 1.4"])
      s.add_runtime_dependency(%q<rack>.freeze, ["< 2", ">= 1.1"])
      s.add_runtime_dependency(%q<haml>.freeze, ["~> 4"])
      s.add_runtime_dependency(%q<haml-magic-translations>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<sass>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<maruku>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<fast_gettext>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<lockfile>.freeze, ["~> 2"])
      s.add_runtime_dependency(%q<json>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<rainbows>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<multipart-parser>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<upr>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<moneta>.freeze, ["< 2", ">= 0.7"])
    else
      s.add_dependency(%q<rake>.freeze, [">= 0"])
      s.add_dependency(%q<rspec>.freeze, ["~> 3"])
      s.add_dependency(%q<timecop>.freeze, [">= 0"])
      s.add_dependency(%q<rack-test>.freeze, [">= 0"])
      s.add_dependency(%q<capybara>.freeze, [">= 0"])
      s.add_dependency(%q<cucumber>.freeze, [">= 0"])
      s.add_dependency(%q<activesupport>.freeze, [">= 0"])
      s.add_dependency(%q<tzinfo>.freeze, [">= 0"])
      s.add_dependency(%q<net-ldap>.freeze, [">= 0"])
      s.add_dependency(%q<gettext>.freeze, ["~> 3"])
      s.add_dependency(%q<bcrypt>.freeze, [">= 0"])
      s.add_dependency(%q<sinatra>.freeze, ["~> 1.4"])
      s.add_dependency(%q<sinatra-contrib>.freeze, ["~> 1.4"])
      s.add_dependency(%q<rack>.freeze, ["< 2", ">= 1.1"])
      s.add_dependency(%q<haml>.freeze, ["~> 4"])
      s.add_dependency(%q<haml-magic-translations>.freeze, [">= 0"])
      s.add_dependency(%q<sass>.freeze, [">= 0"])
      s.add_dependency(%q<maruku>.freeze, [">= 0"])
      s.add_dependency(%q<fast_gettext>.freeze, [">= 0"])
      s.add_dependency(%q<lockfile>.freeze, ["~> 2"])
      s.add_dependency(%q<json>.freeze, [">= 0"])
      s.add_dependency(%q<rainbows>.freeze, [">= 0"])
      s.add_dependency(%q<multipart-parser>.freeze, [">= 0"])
      s.add_dependency(%q<upr>.freeze, [">= 0"])
      s.add_dependency(%q<moneta>.freeze, ["< 2", ">= 0.7"])
    end
  else
    s.add_dependency(%q<rake>.freeze, [">= 0"])
    s.add_dependency(%q<rspec>.freeze, ["~> 3"])
    s.add_dependency(%q<timecop>.freeze, [">= 0"])
    s.add_dependency(%q<rack-test>.freeze, [">= 0"])
    s.add_dependency(%q<capybara>.freeze, [">= 0"])
    s.add_dependency(%q<cucumber>.freeze, [">= 0"])
    s.add_dependency(%q<activesupport>.freeze, [">= 0"])
    s.add_dependency(%q<tzinfo>.freeze, [">= 0"])
    s.add_dependency(%q<net-ldap>.freeze, [">= 0"])
    s.add_dependency(%q<gettext>.freeze, ["~> 3"])
    s.add_dependency(%q<bcrypt>.freeze, [">= 0"])
    s.add_dependency(%q<sinatra>.freeze, ["~> 1.4"])
    s.add_dependency(%q<sinatra-contrib>.freeze, ["~> 1.4"])
    s.add_dependency(%q<rack>.freeze, ["< 2", ">= 1.1"])
    s.add_dependency(%q<haml>.freeze, ["~> 4"])
    s.add_dependency(%q<haml-magic-translations>.freeze, [">= 0"])
    s.add_dependency(%q<sass>.freeze, [">= 0"])
    s.add_dependency(%q<maruku>.freeze, [">= 0"])
    s.add_dependency(%q<fast_gettext>.freeze, [">= 0"])
    s.add_dependency(%q<lockfile>.freeze, ["~> 2"])
    s.add_dependency(%q<json>.freeze, [">= 0"])
    s.add_dependency(%q<rainbows>.freeze, [">= 0"])
    s.add_dependency(%q<multipart-parser>.freeze, [">= 0"])
    s.add_dependency(%q<upr>.freeze, [">= 0"])
    s.add_dependency(%q<moneta>.freeze, ["< 2", ">= 0.7"])
  end
end

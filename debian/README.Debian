coquelicot for Debian
=====================

Coquelicot should be ready to be tested out of the box. Point a browser at
<http://127.0.0.1:51161/> to make sure of it. The default password is "test".

To use Coquelicot in production, you will need at least do two things: adjust
the authentication method and setup a non-buffering HTTPS reverse proxy.

Authentication method
---------------------

To adjust the authentication method, edit the `authentication_method` options
at the bottom of `/etc/coquelicot/settings.yml`. More details about the
supported authentication methods can be found in
`/usr/share/doc/coquelicot/examples/settings-simplepass.yml` for the pre-shared
password, `/usr/share/doc/coquelicot/examples/settings-imap.yml` for
authentication against an IMAP server, and
`/usr/share/doc/coquelicot/examples/settings-ldap.yml` for authentication
against an LDAP server.

Non-buffering HTTPS reverse proxy
---------------------------------

Coquelicot has been successfully tested behind the following reverse proxies:

     Debian package | Example configuration file
    ----------------|--------------------------------------------------------
     apache2        | /usr/share/doc/coquelicot/examples/coquelicot.apache2
     nginx          | /usr/share/doc/coquelicot/examples/coquelicot.nginx
     pound          | /usr/share/doc/coquelicot/examples/coquelicot.pound

Coquelicot can also be configured as a “sub-directory”. To do so, `path` needs
to be set in `settings.yml` to the desired value. For Apache, you then have
to add to your VirtualHost something like:

    <Location /coquelicot>
            ProxyPass http://127.0.0.1:51161/coquelicot
            SetEnv proxy-sendchunks 1
            RequestHeader set X-Forwarded-SSL "on"
    </Location>

Extra configuration
-------------------

Other configuration settings can be modified in
`/etc/coquelicot/settings.yml`. The default configuration can be seen
in `/usr/share/doc/coquelicot/examples/settings-default.yml`.

Changes to `pid_file` must be reflected in `/etc/init.d/coquelicot`.
Likewise, changes to `log` must be reflected in
`/etc/logrotate.d/coquelicot`.

Migrate from Jyraphe
--------------------

[Jyraphe] is another free software web file sharing application.
Coquelicot provides a migration script to import Jyraphe 0.5
repositories as `coquelicot migrate-jyraphe`:

    Usage: coquelicot [options] migrate-jyraphe \ 
                      [command options] JYRAPHE_VAR > REWRITE_RULES

    Options:
        -c, --config FILE            read settings from FILE

    Command options:
        -p, --rewrite-prefix PREFIX  prefix URL in rewrite rules

The last argument must be a path to the `var` directory of the Jyraphe
installation. After migrating the files to Coquelicot, directives for
Apache mod_rewrite will be printed on stdout which ought to be
redirected to a file. Using the `-p` option will prefix URL with the
given path in the rewrite rules.

[Jyraphe]: http://home.gna.org/jyraphe/

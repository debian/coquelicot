Source: coquelicot
Section: web
Priority: optional
Maintainer: Jérémy Bobbio <lunar@debian.org>
Build-Depends: cucumber,
               debhelper (>= 9),
               dh-systemd,
               gem2deb (>= 0.3.0~),
               rainbows,
               rake,
               ruby-activesupport,
               ruby-bcrypt,
               ruby-capybara,
               ruby-fast-gettext,
               ruby-gettext,
               ruby-haml (>= 4),
               ruby-haml-magic-translations,
               ruby-json,
               ruby-lockfile,
               ruby-maruku,
               ruby-moneta (>= 0.7),
               ruby-multipart-parser,
               ruby-net-ldap,
               ruby-rack,
               ruby-rack-test,
               ronn,
               ruby-rspec (>= 3),
               ruby-sass,
               ruby-sinatra (>= 1.4),
               ruby-sinatra-contrib (>= 1.4),
               ruby-timecop,
               ruby-tzinfo,
               ruby-upr
Standards-Version: 3.9.8
Vcs-Git: https://salsa.debian.org/debian/coquelicot.git
Vcs-Browser: https://salsa.debian.org/debian/coquelicot
Homepage: https://coquelicot.potager.org/
XS-Ruby-Versions: all

Package: coquelicot
XB-Ruby-Versions: ${ruby:Versions}
Architecture: all
Depends: adduser,
         libjs-jquery,
         lsb-base (>= 3.2-14),
         rainbows,
         ruby-bcrypt,
         ruby-fast-gettext,
         ruby-haml (>= 4),
         ruby-haml-magic-translations,
         ruby-json,
         ruby-lockfile,
         ruby-maruku,
         ruby-moneta (>= 0.7),
         ruby-multipart-parser,
         ruby-net-ldap,
         ruby-rack,
         ruby-sass,
         ruby-sinatra (>= 1.4),
         ruby-sinatra-contrib,
         ruby-upr,
         ruby | ruby-interpreter,
         sysvinit-utils (>= 2.88dsf-50),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: apache2 | nginx | pound
Built-Using: ${coquelicot:Built-Using}
Description: "one-click" file sharing web application with a focus on users' privacy
 Coquelicot is a "one-click" file sharing web application with a focus
 on protecting users' privacy.
 .
 Basic principle: users can upload a file to the server, in return they
 get a unique URL which can be shared with others in order to download
 the file.
 .
 Coquelicot aims to protect, to some extent, users and system
 administrators from disclosure of the files exchanged from passive and
 not so active attackers.
 .
 Coquelicot should be installed behind a non-buffering HTTPS reverse proxy
 like Apache, nginx or pound.

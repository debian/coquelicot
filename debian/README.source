Debian packaging notes for Coquelicot
=====================================

Debian packaging for Coquelicot is managed in a Git repository using
git-buildpackage.

Packaging practices also follow most of the advices provided by Russ
Allbery at: <http://www.eyrie.org/~eagle/notes/debian/git.html>

Quilt patches are managed using gbp-pq.

Here is how to setup a work environment:

    gbp clone git+ssh://git.debian.org/collab-maint/coquelicot.git
    cd coquelicot
    git remote add potager-upstream https://coquelicot.potager.org/coquelicot.git

Once this is done, upgrading to a new version should look like:

    VERSION=0.9.3
    git fetch potager-upstream
    git tag -v coquelicot-$VERSION
    uscan --verbose
    gbp import-orig --upstream-vcs-tag=coquelicot-$VERSION ../coquelicot-$VERSION.tar.gz
    [… Adapt as needed, refresh debian/patches …]
    [… Apply all patches using quilt …]
    debian/rules create-static-gemspec
    [… Unapply all patches using quilt …]
    dch -v $VERSION-1
    gbp buildpackage
    [… Test, hack, build, repeat …]
    dch --release
    git commit -m "Release version $VERSION-1" debian/changelog
    gbp buildpackage
    [… Check and test that everything is good once more …]
    git tag -s -F ../build-area/coquelicot_$VERSION-1_*.changes debian/$VERSION-1
